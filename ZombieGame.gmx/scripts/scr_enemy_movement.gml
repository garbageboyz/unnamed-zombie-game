/**
 * scr_enemy_movement.gml
 * Code to have enemy follow player when player is within certain range
 *
 * Authors: Kevin Poretti & Zach Danski
 */

 image_speed = 0.2;
 
 if (!attacking)
 {
 
    var closest_player = instance_nearest(x, y, obj_player);


    var diff_x = closest_player.x - x;
    var diff_y = y - closest_player.y;

    angle = arctan2(diff_y, diff_x);
    direction = radtodeg(angle);

    if distance_to_object(closest_player) <= MAX_FOLLOW_RANGE
    {
        motion_set(direction, ENEMY_SPEED);
        image_speed = speed/10;
    }
    else 
    {
        motion_set(direction, 0);
        image_speed = 0;
    }


    var max_diff = 360;
    var closest_angle = 0.0;
    var i;
    for (i = 0; i < ds_list_size(ANGLE_LIST); i += 1)
    {
        angle_diff = abs(ANGLE_LIST[|i] - direction);
        if (angle_diff < max_diff)
        {
            max_diff = angle_diff;
            closest_angle = ANGLE_LIST[|i];
        }
    } 

    //change the sprite      
    if (closest_angle == 0)
    {
        sprite_index = spr_enemy_walk_east;
    }
    else if (closest_angle == 45.0)  
    {
        sprite_index = spr_enemy_walk_northeast;
    }
    else if (closest_angle == 90.0)  
    {
        sprite_index = spr_enemy_walk_north;
    }
    else if (closest_angle == 135.0)  
    {
        sprite_index = spr_enemy_walk_northwest;
    }
    else if (closest_angle == 180)
    {   
        sprite_index = spr_enemy_walk_west;
    }
    else if (closest_angle == 225)
    {
        sprite_index = spr_enemy_walk_southwest;
    }
    else if (closest_angle == 270)
    {
        sprite_index = spr_enemy_walk_south;
    }
    else if (closest_angle == 315)
    {
    sprite_index = spr_enemy_walk_southeast;
    }
}

else
{
    speed = 0;
}

