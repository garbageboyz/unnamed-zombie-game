/**
 * scr_enemy_attack.gml
 * Handles initiating attack when a player is in range and creating collision boxes
 *
 * Author: Zach Danski
 */

// Initiation of attack 
var closest_player = instance_nearest(x, y, obj_player);
if(distance_to_object(closest_player) < 20 && !attacking)
{
    timeline_index = tml_enemy_attack;
    timeline_position = 0;
    timeline_loop = false;
    timeline_running = true;
}
