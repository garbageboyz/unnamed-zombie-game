/**
 * scr_warrior_movement.gml
 * Handles movement (changing sprites, setting speed, etc.)
 *
 * Author: Kevin Poretti
 */

image_speed = 0.2; 
 
if (!attacking)
{
    if (keyboard_check(ord("W")) && keyboard_check(ord("D")))  
    {   
        direction = 45.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_northeast;
    }
    else if (keyboard_check(ord("W")) && keyboard_check(ord("A")))  
    {
        direction = 135.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_northwest;
    }
    else if (keyboard_check(ord("S")) && keyboard_check(ord("A")))  
    {
        direction = 225.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_southwest; 
    }
    else if (keyboard_check(ord("S")) && keyboard_check(ord("D")))  
    {
        direction = 315.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_southeast;
    }
    else if (keyboard_check(ord("W"))) 
    {   
        direction = 90.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_north;
    }
    else if (keyboard_check(ord("A"))) 
    {
        direction = 180.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_west;
    }
    else if (keyboard_check(ord("S"))) 
    {
        direction = 270.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_south;
    }
    else if (keyboard_check(ord("D"))) 
    {
        direction = 0.0;
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        sprite_index = spr_war_walk_east;
    }
    else
    {
        if(direction == 0)
        {
            sprite_index = spr_war_idle_east;
        }
        else if(direction == 45)  
        {
            sprite_index = spr_war_idle_northeast;
        }
        else if(direction == 90)  
        {
            sprite_index = spr_war_idle_north;
        }
        else if(direction == 135)  
        {
            sprite_index = spr_war_idle_northwest;
        }
        else if(direction == 180) 
        {   
            sprite_index = spr_war_idle_west;
        }
        else if(direction == 225) 
        {
            sprite_index = spr_war_idle_southwest;
        }
        else if(direction == 270) 
        {
            sprite_index = spr_war_idle_south;
        }
        else if(direction == 315)
        {
            sprite_index = spr_war_idle_southeast;
        }
        speed = clamp(speed-ACCEL, 0, MAX_MOVEMENT_SPEED);
    }
}
else
{
    speed = 0;
}
