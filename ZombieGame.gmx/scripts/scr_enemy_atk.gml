/**
 * scr_enemy_attack.gml
 * Handles initiating attack on button press and creating collision boxes
 *
 * Author: Zach Danski
 */

// Initiation of attack 
var closest_player = instance_nearest(x, y, obj_player);
if(distance_to_object(closest_player) < 20 && !attacking)
{
    timeline_index = tml_enemy_atk;
    timeline_position = 0;
    timeline_loop = false;
    timeline_running = true;
}
