/**
 * scr_warrior_attack.gml
 * Handles initiating attack on button press and creating collision boxes
 *
 * Author: Kevin Poretti
 */

// Initiation of attack on button press
if(keyboard_check_pressed(ord('J')) && !attacking)
{
    timeline_index = tml_war_atk;
    timeline_position = 0;
    timeline_loop = false;
    timeline_running = true;
}

