/**
 * scr_find_direction.gml
 * Finds the angle given a x_dir and y_dir variable
 * x_dir and y_dir both take on values -1, 0, or 1
 * -1 => left or down 
 *  1 => right or up 
 *  0 => no movement in that particular direction
 *
 * Author: Kevin Poretti
 */
 
var x_dir      = argument0;
var y_dir      = argument1;
var dir        = 0.0;
//var player_spr = argument3;

if((x_dir > 0) && (y_dir > 0))
{
    dir = 45.0;
}
else if((x_dir > 0) && (y_dir < 0))
{
    dir = 315.0;
}
else if((x_dir > 0) && (y_dir == 0))
{
    dir = 0.0;
}
else if((x_dir < 0) && (y_dir > 0))
{
    dir = 135.0;
}
else if((x_dir < 0) && (y_dir < 0))
{
    dir = 225.0;
}
else if((x_dir < 0) && (y_dir = 0))
{
    dir = 180.0;
}
else if((x_dir = 0) && (y_dir > 0))
{
    dir = 90.0;
}
else if((x_dir = 0) && (y_dir < 0))
{
    dir = 270.0;
}

return dir;



    
