/**
 * scr_warrior_movement.gml
 * Handles movement (changing sprites, setting speed, etc.)
 *
 * Author: Kevin Poretti
 */
 
var x_dir = keyboard_check(ord("D")) - keyboard_check(ord("A"));
var y_dir = keyboard_check(ord("W")) - keyboard_check(ord("S"));

if (!attacking)
{
    if((x_dir != 0) || (y_dir != 0))    //player walking
    {
        scr_change_direction(x_dir, y_dir, self);
        speed = clamp(speed+ACCEL, 0, MAX_MOVEMENT_SPEED);
        image_speed = speed/10;
    }
    else
    {
        speed = clamp(speed-ACCEL, 0, MAX_MOVEMENT_SPEED);
        image_speed = speed/10;
    }
}
else
{
    speed = 0;
}
