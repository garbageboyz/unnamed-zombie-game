/**
 * scr_enemy_direction.gml
 * Defines closest angle to find enemy direction
 *
 * Author: Zach Danski
 */

var closest_player = instance_nearest(x, y, obj_player);


var diff_x = closest_player.x - x;
var diff_y = y - closest_player.y;

angle = arctan2(diff_y, diff_x);
direction = radtodeg(angle);

var max_diff = 360;
var closest_angle = 0.0;
var i;
for (i = 0; i < ds_list_size(ANGLE_LIST); i += 1)
{
    angle_diff = abs(ANGLE_LIST[|i] - direction);
    if (angle_diff < max_diff)
    {
        max_diff = angle_diff;
        closest_angle = ANGLE_LIST[|i];
    }
}
