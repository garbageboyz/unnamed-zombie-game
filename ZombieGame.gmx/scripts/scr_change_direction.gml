/**
 * scr_find_direction.gml
 * Finds the angle given a x_dir and y_dir variable
 * x_dir and y_dir both take on values -1, 0, or 1
 * -1 => left or down 
 *  1 => right or up 
 *  0 => no movement in that particular direction
 *
 * Author: Kevin Poretti
 */
 
var x_dir       = argument0;
var y_dir       = argument1;
var player_inst = argument2;

if((x_dir > 0) && (y_dir > 0))
{
    player_inst.direction = 45.0;
    player_inst.sprite_index = spr_war_walk_northeast;
}
else if((x_dir > 0) && (y_dir < 0))
{
    player_inst.direction = 315.0;
    player_inst.sprite_index = spr_war_walk_southeast;
}
else if((x_dir > 0) && (y_dir == 0))
{
    player_inst.direction = 0.0;
    player_inst.sprite_index = spr_war_walk_east;
}
else if((x_dir < 0) && (y_dir > 0))
{
    player_inst.direction = 135.0;
    player_inst.sprite_index = spr_war_walk_northwest;
}
else if((x_dir < 0) && (y_dir < 0))
{
    player_inst.direction = 225.0;
    player_inst.sprite_index = spr_war_walk_southwest;
}
else if((x_dir < 0) && (y_dir = 0))
{
    player_inst.direction = 180.0;
    player_inst.sprite_index = spr_war_walk_west;
}
else if((x_dir = 0) && (y_dir > 0))
{
    player_inst.direction = 90.0;
    player_inst.sprite_index = spr_war_walk_north;
}
else if((x_dir = 0) && (y_dir < 0))
{
    player_inst.direction = 270.0;
    player_inst.sprite_index = spr_war_walk_south;
}



    
